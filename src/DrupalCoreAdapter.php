<?php

namespace Drupal\d3_sankey;

/**
 * Adapter class to provide a mockable wrapper around D7 procedural functions.
 *
 * Drupal's procedural functions are hard to use in automated unit tests without
 * bootstraping Drupal, which is computationally and temporally expensive, and
 * would use a lot of memory, especially if you really only need one or two
 * functions from Drupal core.
 *
 * This adapter class provides a thin wrapper around certain Drupal core
 * functions which are used in the object-oriented code in this module. These
 * functions are intentionally _not_ static, because PHPUnit does not
 * (currently) let you mock static functions.
 *
 * This class is intended to be passed to a class that needs it in the class'
 * constructor. The following template is recommended...
 *
 * ```
 * public function __construct(DrupalCoreAdapter $adapter = NULL) {
 *   // ...
 *   $this->adapter = ($adapter) ? $adapter : new DrupalCoreAdapter();
 *   // ...
 * }
 * ```
 *
 * ... this allows regular code to call the constructor without providing an
 * adapter object. Meanwhile, in the test, you can construct a test double
 * (either by extending this object and providing an alternate implementation,
 * or by using PHPUnit's mocking tools to create a dummy, stub, fake, spy, or
 * mock). For example, the following code defines a fake method that simply
 * returns the argument it was passed...
 *
 * ```
 * $this->mockAdapter = $this->createMock(DrupalCoreAdapter::class);
 * $this->mockAdapter->method('drupalHtmlId')->willReturnArgument(0);
 * ```
 *
 * If you are writing your own preprocessor, feel free to extend this object if
 * your own code makes use of other functions in Drupal core.
 */
class DrupalCoreAdapter {

  /**
   * Prepares a string for use as a valid HTML ID and guarantees uniqueness.
   *
   * @see drupal_html_id()
   */
  public function drupalHtmlId($id) {
    return drupal_html_id($id);
  }

}
