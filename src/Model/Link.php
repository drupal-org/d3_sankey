<?php

namespace Drupal\d3_sankey\Model;

/**
 * An object representing a single, raw Sankey link.
 */
class Link {

  /**
   * The array index of the node to connect from.
   *
   * @var int
   */
  public $source;

  /**
   * The array index of the node to connect to.
   *
   * @var int
   */
  public $target;

  /**
   * A number representing how big the link is.
   *
   * @var numeric
   */
  public $value;

  /**
   * D3SankeyLink constructor.
   *
   * @param int $source
   *   The array index of the node to connect from.
   * @param int $target
   *   The array index of the node to connect to.
   * @param int|float $value
   *   A number representing how big the link is.
   */
  public function __construct($source, $target, $value = 1) {
    $this->source = (int) $source;
    $this->target = (int) $target;
    $this->value = is_numeric($value) ? $value + 0 : 1;
  }

  /**
   * Get an associated-array version of this link.
   *
   * @return array
   *   An associative array containing:
   *   - source: The array index of the node to connect from.
   *   - target: The array index of the node to connect to.
   *   - value (optional): A number representing how big the link is.
   */
  public function toAssocArray() {
    $answer = array('source' => $this->source, 'target' => $this->target);

    if (!is_null($this->value)) {
      $answer['value'] = $this->value;
    }

    return $answer;
  }

}
