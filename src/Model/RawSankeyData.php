<?php

namespace Drupal\d3_sankey\Model;

/**
 * A container class for raw Sankey data.
 *
 * The raw node and link data used by the Sankey chart plugin for the D3 library
 * is very closely related: if you add, rearrange, or delete nodes, you also
 * have to change all the links, and vice-versa.
 *
 * This close inter-relatedness makes the data difficult to work with. It would
 * be useful to be able to create some sort of preprocessor to turn data that is
 * easier to work with into the raw data that the Sankey chart plugin for the D3
 * library needs.
 *
 * In order to do make a preprocessor, we need a way to return the nodes and
 * links _at the same time_, in a single chunk (a 2-tuple). Besides helping to
 * avoid situations where the Sankey data changes between the time when you get
 * the links and the time when you get the nodes, it makes preprocessor
 * implementation easier by allowing them both to be calculated at the same
 * time.
 *
 * This class is designed to contain both the node and link data in a single
 * chunk, with a few extra convenience functions.
 */
class RawSankeyData {

  /**
   * An array of nodes that make up this Sankey diagram.
   *
   * @var \Drupal\d3_sankey\Model\Node[]
   */
  private $nodes;

  /**
   * An array of links that make up this Sankey diagram.
   *
   * @var \Drupal\d3_sankey\Model\Link[]
   */
  private $links;

  /**
   * RawSankeyData constructor.
   *
   * @param \Drupal\d3_sankey\Model\Node[] $nodes
   *   An array of nodes that make up this Sankey diagram.
   * @param \Drupal\d3_sankey\Model\Link[] $links
   *   An array of links that make up this Sankey diagram.
   */
  public function __construct($nodes = array(), $links = array()) {
    $this->nodes = $nodes;
    $this->links = $links;
  }

  /**
   * Get an array of nodes that make up this Sankey diagram.
   *
   * @return \Drupal\d3_sankey\Model\Node[]
   *   An array of nodes that make up this Sankey diagram.
   */
  public function getNodes() {
    return $this->nodes;
  }

  /**
   * Get an array of associative arrays representing the nodes in this raw data.
   *
   * @return array
   *   A numeric array of associative arrays containing:
   *   - name: A label for the node.
   *   - id (optional): An SVG ID attribute (similar to an HTML ID attribute) to
   *     add to the node.
   */
  public function getAssocArrayNodes() {
    $answer = array();

    foreach ($this->nodes as $node) {
      $answer[] = $node->toAssocArray();
    }

    return $answer;
  }

  /**
   * Get an array of links that make up this Sankey diagram.
   *
   * @return \Drupal\d3_sankey\Model\Link[]
   *   An array of links that make up this Sankey diagram.
   */
  public function getLinks() {
    return $this->links;
  }

  /**
   * Get an array of associative arrays representing the links in this raw data.
   *
   * @return array
   *   A numeric array of associative arrays containing:
   *   - name: A label for the node.
   *   - id (optional): An SVG ID attribute (similar to an HTML ID attribute) to
   *     add to the node.
   */
  public function getAssocArrayLinks() {
    $answer = array();

    foreach ($this->links as $link) {
      $answer[] = $link->toAssocArray();
    }

    return $answer;
  }

  /**
   * Set the array of nodes that make up this Sankey diagram.
   *
   * @param \Drupal\d3_sankey\Model\Node[] $nodes
   *   An array of nodes that make up this Sankey diagram.
   */
  public function setNodes($nodes) {
    $this->nodes = $nodes;
  }

  /**
   * Set the array of nodes that make up this Sankey diagram.
   *
   * @param \Drupal\D3Sankey\Model\Link[] $links
   *   An array of links that make up this Sankey diagram.
   */
  public function setLinks($links) {
    $this->links = $links;
  }

}
