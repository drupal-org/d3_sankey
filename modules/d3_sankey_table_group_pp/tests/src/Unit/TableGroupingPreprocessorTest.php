<?php

namespace Drupal\Tests\D3SankeyGroupingPreprocess;

use Drupal\d3_sankey\DrupalCoreAdapter;
use Drupal\d3_sankey\Model\Link;
use Drupal\d3_sankey\Model\Node;
use Drupal\d3_sankey\Model\RawSankeyData;
use Drupal\d3_sankey_table_group_pp\TableGroupingPreprocessor;
use Drupal\d3_sankey_table_group_pp\Model\KeyedLink;
use PHPUnit\Framework\TestCase;

/**
 * Tests for \Drupal\d3_sankey_table_group_pp\GroupingPreprocessor.
 */
class TableGroupingPreprocessorTest extends TestCase {

  /**
   * A test double for a wrapper around Drupal core functions.
   *
   * @var \PHPUnit_Framework_MockObject_MockObject
   */
  private $mockAdapter;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    // Create a test double for the wrapper around Drupal core functions.
    $this->mockAdapter = $this->createMock(DrupalCoreAdapter::class);

    // Mock the drupalHtmlId() method that returns the string it was passed.
    $this->mockAdapter->method('drupalHtmlId')->willReturnArgument(0);
  }

  /**
   * Test that the preprocessor will return raw data in the correct format.
   */
  public function testGetRawData() {
    // Prepare some test data.
    // Note that we're specifically _not_ testing the implementation details of
    // generateNodeKey() and generateLinkKey() by using keys that would not
    // normally be generated by those functions.
    $input_nodes = array(
      'test0' => new Node('foo', 'baz', $this->mockAdapter),
      'test1' => new Node('bar', NULL, $this->mockAdapter),
    );
    $input_links = array(
      'fizz0' => new KeyedLink('test0', 'test1', 314),
    );

    // Initialize the system under test.
    $sut = new TableGroupingPreprocessor(array(), array(), $this->mockAdapter);
    $sut->setNodes($input_nodes);
    $sut->setLinks($input_links);

    // Run the system under test.
    $output = $sut->getRawData();

    // Ensure the output is what we expect.
    $this->assertInstanceOf(RawSankeyData::class, $output,
      'The raw output is a RawSankeyData.');

    // Transform the output data to make it easier for us to run assertions.
    $output_nodes = $output->getNodes();
    $output_links = $output->getLinks();

    // Ensure the output is what we expect.
    $this->assertCount(2, $output_nodes,
      'There are 2 nodes in the output because there were 2 nodes in the input.');

    $this->assertArrayNotHasKey('test0', $output_nodes,
      'The test0 key is not in the node array output by getRawData().');
    $this->assertInstanceOf(Node::class, $output_nodes[0],
      'The first thing in the node output array is a Node.');
    $this->assertEquals('foo', $output_nodes[0]->name,
      'The name of the first output node is foo.');
    $this->assertEquals('baz', $output_nodes[0]->id,
      'The id of the first output node is baz.');

    $this->assertArrayNotHasKey('test1', $output_nodes,
      'The test1 key is not in the node array output by getRawData().');
    $this->assertInstanceOf(Node::class, $output_nodes[1],
      'The second thing in the node output array is a Node.');
    $this->assertEquals('bar', $output_nodes[1]->name,
      'The name of the first output node is bar.');

    $this->assertCount(1, $output_links,
      'There is 1 link in the output because there was 1 link in the input.');

    $this->assertArrayNotHasKey('fizz0', $output_links,
      'The fizz0 key is not in the link array output by getRawData().');
    $this->assertInstanceOf(Link::class, $output_links[0],
      'The first thing in the output array of links is a Link or one of its sub-types..');
    $this->assertNotInstanceOf(KeyedLink::class, $output_links[0],
      'The first thing in the output array of links is not a GroupingLink.');
    $this->assertEquals(0, $output_links[0]->source,
      'The source of the first output link is 0.');
    $this->assertEquals(1, $output_links[0]->target,
      'The target of the first output link is 1.');
    $this->assertEquals(314, $output_links[0]->value,
      'The value of the first output link is 314.');
  }

  /**
   * Test that the preprocessor will ingest rows of data properly.
   */
  public function testIngestRow() {
    // Initialize the system under test.
    $sut = new TableGroupingPreprocessor(array(), array(), $this->mockAdapter);

    // Prepare some test data.
    $input_data_0 = array(
      array('Revenue', 'Service level agreements', 'Client 1'),
      array('Revenue', 'New work', 'Client 1'),
      array('Revenue', 'Tech support', 'Client 1'),
    );

    // Run the system under test.
    foreach ($input_data_0 as $row) {
      $sut->ingestRow($row);
    }

    // Ensure the output is what we expect.
    $this->assertCount(5, $sut->getNodes(),
      'There are 5 unique nodes, because there were 5 unique strings in the initial ingested data.');
    $this->assertCount(6, $sut->getLinks(),
      'There are 6 unique links, because there were 6 unique connections in the initial ingested data.');

    // Prepare some additional test data.
    $input_data_1 = array(
      array('Revenue', 'Service level agreements', 'Client 2'),
      array('Revenue', 'New work', 'Client 2'),
      array('Revenue', 'Tech support', 'Client 2'),
    );

    // Run the system under test again.
    foreach ($input_data_1 as $row) {
      $sut->ingestRow($row);
    }

    // Ensure the new output is what we expect.
    $this->assertCount(6, $sut->getNodes(),
      'There are 6 unique nodes, because there were 6 unique strings in the combined ingested data.');
    $this->assertCount(9, $sut->getLinks(),
      'There are 9 unique links, because there were 9 unique connections in the combined ingested data.');
  }

}
