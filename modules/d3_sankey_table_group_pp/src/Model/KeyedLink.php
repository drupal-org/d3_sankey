<?php

namespace Drupal\d3_sankey_table_group_pp\Model;

use Drupal\d3_sankey\Model\Link;

/**
 * A type of Sankey link that supports strings for source and target.
 */
class KeyedLink extends Link {

  /**
   * The array key of the node to connect from.
   *
   * @var string
   */
  public $source;

  /**
   * The array key of the node to connect to.
   *
   * @var string
   */
  public $target;

  /**
   * A number representing how big the link is.
   *
   * @var numeric
   */
  public $value;

  /**
   * D3SankeyLink constructor.
   *
   * @param string $source
   *   The array index of the node to connect from.
   * @param string $target
   *   The array index of the node to connect to.
   * @param int|float $value
   *   A number representing how big the link is.
   */
  public function __construct($source, $target, $value = 1) {
    $this->source = (string) $source;
    $this->target = (string) $target;
    $this->value = is_numeric($value) ? $value + 0 : 1;
  }

}
